import os
import random
import string
from click import password_option
from flask import Flask,Blueprint, render_template, redirect, request, flash, url_for, session
from flask_login import LoginManager, login_user, current_user, login_required
from flask_session import Session
from dbclasses import *
from datetime import date, datetime, timedelta
from flask_login import logout_user
from datetime import date
import hashlib
from flask_sqlalchemy import SQLAlchemy

"""
COSE DA FARE:
Creazione Thread /
Creazione Post /
Controllo del legame fra i post
Gestione Profilo(Admin/User)
Interfaccia
"""

app = Flask(__name__)


letters = string.ascii_lowercase
"4ou3f2ou43reObheoif8t384984yfr8g"

app.config['SESSION_TYPE'] = 'filesystem'
app.config['SECRET_KEY'] =''.join(random.choice(letters) for i in range(10))
#app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://luca:luca@localhost/forum'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://luca:4ou3f2ou43reObheoif8t384984yfr8g@gabally.net/forum'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config.from_object(__name__)
# IMPORTANTE
db.init_app(app)

with app.app_context():
    db.create_all()
Session(app)

login_manager = LoginManager()
login_manager.init_app(app)



@login_manager.user_loader
def load_user(user_id):
    return User.query.get(user_id)


@app.route('/')
def index():
    return redirect('/home')


@app.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        usrn = request.form.get('username')
        password = request.form.get('password')
        user = User.query.filter_by(username=usrn).first()
        if user:
            if user.password == encodePass(password):
                login_user(user, remember=True)
                flash('Loggato!', category='success')
                return redirect(url_for('home'))
            else:
                flash('Password sbagliata.', category='error')
        else:
            print("username="+usrn+" password"+password)
            #print("Query:  "+user)
            flash('Mail o nome inesistente.', category='error')

    return render_template("login.html", user=current_user)


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/profilo/<id>', methods=['GET','POST'])
def userPro(id):
    us=User.query.get(id)
    tList=Threads.query.all()
    print(us)
    return render_template("profilo.html",user=us,tl=tList)

@app.route('/sign-up', methods=['GET','POST'])
def sign_up():
    if request.method == 'POST':
        password = request.form.get('password1')
        cpassword = request.form.get('password2')
        if password != cpassword:
            flash('Cè qualcosa che non va.', category='error')
        elif len(password)<4:
            flash('Password troppo corta', category='error')
        else:
            user = User(
                mail = request.form.get('email'),
                name = request.form.get('name'),
                surname = request.form.get('surname'),
                username = request.form.get('username'),
                password = encodePass(request.form.get('password1')),
                role="user"
            )
            user.set_password(request.form.get('password1'))
            db.session.add(user)
            db.session.commit()
            flash("Registrazione riuscita", category='success')
            login_user(user, remember=True)
            return redirect(url_for('home'))

    return render_template("signup.html",user=current_user)

@app.route('/test/', methods=['GET','POST','DELETE'])
def test():
    threadList=Threads.query.all()
    return render_template("test.html",user=current_user,tList=threadList)

@app.route('/home/', methods=['GET','POST','DELETE'])
def home():
    if(request.form.get('filtro')):
        threadList=Threads.query.filter_by(name=request.form.get('filtro'))
    else:
        threadList=Threads.query.order_by(Threads.lastUpdate.desc()).all()
        print('ciao')
    postList=Posts.query.all()
    userList=User.query.all()
    print("Prova del ruolo: ", current_user.is_authenticated)
    
    """
    for u in userList:
        for p in u.posts:
            print(p.content)
    for t in threadList:
        print(t.user)
    """

    if request.form.get('testoInserito'):
        if ( current_user.get_id()==None ):
            return redirect('/login')
        else:
            testo = request.form.get('testoInserito')
            if testo!= '':
                thread=Threads(name = testo,user_id = current_user.id,creationDate=datetime.now(),lastUpdate=datetime.now())
                db.session.add(thread)
                db.session.commit()
                flash ("Thread creato ", category='success')
                return redirect(request.referrer)
    return render_template("home.html",user=current_user,prova="ciao",tList=threadList,pList=postList,uList=userList)

@app.route('/delete/<id>', methods=['GET','DELETE'])
@login_required
def delete(id):
    #threadList=Threads.query.all()
    td = Threads.query.get(id)
    db.session.delete(td)
    db.session.commit()
    return redirect(url_for('home'))

@app.route('/deletecom/<idcom>/<idthread>/<tname>', methods=['GET','DELETE'])
@login_required
def deletecom(idcom,idthread,tname):
    #threadList=Threads.query.all()
    p = Posts.query.get(idcom)
    db.session.delete(p)
    db.session.commit()
    return redirect(url_for('see_thread', id= idthread ,threadName = tname))

@app.route('/thread/<id>', methods=['GET','POST'])
@login_required
def see_thread(id):
    print(id)
    tn=Threads.query.filter(Threads.id==id).first()
    print(tn.creationDate.date())
    pList=Posts.query.filter(Posts.thread_id==id)
    userList=User.query.all()
    if request.form.get('commentoInserito'):
        if ( current_user.get_id()==None ):
            return redirect('/login')
        else:
            testo = request.form.get('commentoInserito')
            if testo!= '':
                post=Posts(content = testo,user_who_posted_id = current_user.id,postDate=datetime.now(),thread_id=id,is_bound=0)
                db.session.query(Threads).filter(Threads.id == id).update({Threads.lastUpdate: datetime.now()})
                db.session.add(post)
                db.session.commit()
                flash ("Post creato ", category='success')
                return redirect(request.referrer)

    if request.form.get('rispostaInserita'):
        if ( current_user.get_id()==None ):
            return redirect('/login')
        else:
            testo = request.form.get('rispostaInserita')
            if testo!= '':
                print(request.form.get('hidden'))
                postTime=datetime.now()
                reply=Replies(content=testo, user_id = current_user.id,postDate=postTime,thread_id=id,post_id=request.form.get('hidden'))
                db.session.add(reply)
                idToUse=Replies.query.filter(Replies.content==testo)
                for i in idToUse:
                    a=i.id
                #print(idToUse)
                binding=Bindings(original_post_id= request.form.get('hidden'), post_reply_id=a)
                db.session.add(binding)
                db.session.commit()
                flash ("Post creato ", category='success')
                return redirect(request.referrer)
    return render_template("threads.html",user=current_user,posts=pList,uL=userList,t=tn,dt=tn.creationDate.date())

@app.route('/aa')
def aa():
    print ("Hello")
    return ("nothing")

def encodePass(passw):
    return hashlib.md5(passw.encode()).hexdigest()


if __name__ == "main":
    app.run(debug=True)





"""

@app.route('/login', methods=['GET','POST'])
def login():
    if request.method == 'POST':
        usrn = request.form.get('username')
        password = request.form.get('password')
        user = session.query(Users).filter_by(username=usrn).first()
        if user:
            if user.password == encodePass(password):
                login_user(user, remember=True)
                flash('Loggato!', category='success')
                return redirect(url_for('views.home'))
            else:
                flash('Password sbagliata.', category='error')
        else:
            flash('Mail inesistente.', category='error')

    return render_template("login.html", user=current_user)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))

@app.route('/sign-up', methods=['GET','POST'])
def sign_up():
    if request.method == 'POST':
        email = request.form.get('email')
        name = request.form.get('name')
        surname = request.form.get('surname')
        username = request.form.get('username')
        password = request.form.get('password1')
        cpassword = request.form.get('password2')
        if password != cpassword:
            flash('Cè qualcosa che non va.', category='error')
        elif len(password)<4:
            flash('Password troppo corta', category='error')
        else:
            insertUser(name, surname, username, email, password, "user")
            login_user(user, remember=True)
            flash("papapapa pa papapapa", category='success')
            return redirect(url_for('views.home'))

    return render_template("signup.html",user=current_user)

@app.route('/home')
@login_required
def home():
    #threadList=getTableContent(threads) #
    threadList=Threads.query.all()
    for t in threadList:
        print(t)
    return render_template("home.html",user=current_user,prova="ciao",tList=threadList)

@app.route('/thread/<id>')
@login_required
def see_thread(id):
    return render_template("threads.html",user=current_user,posts=getPostsOfThread(id))


def encodePass(passw):
    return hashlib.md5(passw.encode()).hexdigest()
"""
