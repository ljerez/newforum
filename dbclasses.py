from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func
from datetime import datetime

db = SQLAlchemy()

#drop database forum sql
#create database forum
#from app import db/from dbclasses import db

class Threads(db.Model):
    __tablename__ = 'threads'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column('name',db.String(60),unique=True)
    user_id=db.Column('user_id',db.Integer,db.ForeignKey('user.id'))
    creationDate=db.Column('creationDate', db.DateTime, default=func.now())
    lastUpdate=db.Column('lastUpdate', db.DateTime, default=func.now())
    posts = db.relationship("Posts",cascade="all, delete-orphan", backref="threads")


class User(db.Model, UserMixin):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column('name',db.String(25))
    surname = db.Column('surname',db.String(25))
    username = db.Column('username',db.String(20))
    mail = db.Column('mail',db.String(50))
    password = db.Column('password',db.CHAR(32))
    role=db.Column('role',db.String(10))
    threads = db.relationship("Threads", backref="user")
    posts = db.relationship("Posts", backref="user")
    replies = db.relationship("Replies", backref="user")
    def __repr__(self):
        return "<User(name='%s', fullname='%s', nickname='%s')>" % (
                            self.name, self.surname, self.username)
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


class Posts(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column('content',db.String(240))
    user_who_posted_id = db.Column('user_who_posted_id', db.Integer, db.ForeignKey('user.id'))
    postDate = db.Column('postDate', db.DateTime, default=func.now())
    thread_id = db.Column('thread_id',db.Integer,db.ForeignKey('threads.id'))
    is_bound = db.Column('is_bound',db.Boolean(1))
    replies = db.relationship("Replies",cascade="all, delete-orphan")
    def __repr__(self):
        return "<Posts(id='%s', content='%s', postDate='%s')>" % (
                            self.id, self.content, self.postDate)

class Replies(db.Model):
    __tablename__ = 'replies'
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column('content',db.String(240))
    user_id = db.Column('user_who_posted_id', db.Integer, db.ForeignKey('user.id'))
    postDate = db.Column('postDate', db.DateTime, default=func.now())
    thread_id = db.Column('thread_id',db.Integer,db.ForeignKey('threads.id'))
    post_id= db.Column('post_id',db.Integer,db.ForeignKey('posts.id'))
    def __repr__(self):
        return "<Replies(id='%s', content='%s', postDate='%s')>" % (
                            self.id, self.content, self.postDate)


class Bindings(db.Model):
    __tablename__ = 'bindings'

    id = db.Column(db.Integer, primary_key=True)
    original_post_id = db.Column(db.Integer)
    post_reply_id = db.Column(db.Integer, unique=True)

def Joins():
    print(threads.join(Posts))
